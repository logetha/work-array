package bcas.ap.utill;

import bcas.ap.sp.PackMessage;

public class PackHolder {

	int row, col;
	int holes[][];
	int maxHoles;

	public PackHolder(int row, int col) {
		this.row = row;
		this.col = col;
		maxHoles = this.row * this.col;
		createHole();
	}

	private void createHole() {
		holes = new int[row][col];
		// Display dis = new Display();
		// dis.drawArray(row, col);

	}

	public String cheackAvailability(int holeNo) {
		if (holeNo <= maxHoles) {
			return placePack(holeNo);

		} else {
			return PackMessage.tokenNo;

		}
	}

	public String placePack(int holeNo) {
		int tokenRow = holeNo / col;
		int tokenCol = (holeNo % col);
		if (tokenCol > 0) {
			tokenCol -= 1;

		}
		if (checkFree(tokenRow, tokenCol)) {
			holes[tokenRow][tokenCol] = holeNo;
			return PackMessage.success;

		}

		return PackMessage.notFree;

	}

	public boolean checkFree(int tokenRow, int tokenCol) {
		holes[3][1] = 20;
		holes[1][4] = 11;
		holes[0][0] = 1;
		if (holes[tokenRow][tokenCol] == 0) {
			return true;

		}

		return false;
	}

}
