package bcas.array.hm;

public class StaticBlock {
	static {
		System.out.println("content from static-block");
	}

	public static void main(String[] args) {
		System.out.println("message from main method");
	}

}
