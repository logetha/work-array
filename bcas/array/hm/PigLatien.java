package bcas.array.hm;

public class PigLatien {
	final static String PigLatien = "PAA";

	public static String createString(String inputStr) {

		return inputStr.substring(1) + inputStr.charAt(0) + PigLatien;
	}

	public static String breakString(String inputStr) {
		if (inputStr.toUpperCase().endsWith(PigLatien)) {
			int trimsize = PigLatien.length() + 1;

			return inputStr.charAt(inputStr.length() - trimsize) + inputStr.substring(0, inputStr.length());

		} else {
			return "couldn't encript this word";
		}

	}
}
